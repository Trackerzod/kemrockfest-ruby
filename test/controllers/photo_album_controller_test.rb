require 'test_helper'

class PhotoAlbumControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get photo_album_index_url
    assert_response :success
  end

  test "should get show" do
    get photo_album_show_url
    assert_response :success
  end

end
