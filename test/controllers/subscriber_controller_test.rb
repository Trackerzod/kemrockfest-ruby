require 'test_helper'

class SubscriberControllerTest < ActionDispatch::IntegrationTest
  test "should get new," do
    get subscriber_new,_url
    assert_response :success
  end

  test "should get create," do
    get subscriber_create,_url
    assert_response :success
  end

  test "should get update" do
    get subscriber_update_url
    assert_response :success
  end

end
