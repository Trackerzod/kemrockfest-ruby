# == Schema Information
#
# Table name: news
#
#  id                  :integer          not null, primary key
#  name                :string
#  slug                :string
#  date_publication    :date
#  preview_description :text
#  description         :text
#  news_image          :string
#  show                :boolean          default(TRUE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class NewsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get news_index_url
    assert_response :success
  end

  test "should get show" do
    get news_show_url
    assert_response :success
  end

end
