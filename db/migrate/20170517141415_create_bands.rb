class CreateBands < ActiveRecord::Migration[5.0]
  def change
    create_table :bands do |t|
      t.string :name
      t.string :slug
      t.date :date_based
      t.text :description
      t.boolean :show

      t.timestamps
    end
  end
end
