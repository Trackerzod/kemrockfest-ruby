class CreatePhotoAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :photo_albums do |t|
      t.string :name
      t.string :slug
      t.date :date_publication
      t.text :description
      t.string :preview_image
      t.boolean :show, default: true

      t.timestamps
    end
  end
end
