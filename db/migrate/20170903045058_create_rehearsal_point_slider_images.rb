class CreateRehearsalPointSliderImages < ActiveRecord::Migration[5.0]
  def change
    create_table :rehearsal_point_slider_images do |t|
      t.string :image
      t.text :description
      t.belongs_to :rehearsal_point, index: true

      t.timestamps
    end
  end
end
