class CreateConcertToBands < ActiveRecord::Migration[5.0]
  def change
    create_table :concert_to_bands do |t|
      t.references :concert, index: true
      t.references :band, index: true
      t.timestamps
    end
  end
end
