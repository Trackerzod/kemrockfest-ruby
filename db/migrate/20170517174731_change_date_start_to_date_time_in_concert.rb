class ChangeDateStartToDateTimeInConcert < ActiveRecord::Migration[5.0]
  def change
    change_column :concerts, :date_start, :datetime
  end
end
