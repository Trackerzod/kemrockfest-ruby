class AddMemberImageToBandMember < ActiveRecord::Migration[5.0]
  def change
    add_column :band_members, :member_image, :string
  end
end
