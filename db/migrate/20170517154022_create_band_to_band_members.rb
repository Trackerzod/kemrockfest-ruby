class CreateBandToBandMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :band_to_band_members do |t|
      t.references :band, index: true
      t.references :band_member, index: true
      t.timestamps
    end
  end
end
