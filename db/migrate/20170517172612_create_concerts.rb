class CreateConcerts < ActiveRecord::Migration[5.0]
  def change
    create_table :concerts do |t|
      t.string :name
      t.string :slug
      t.date :date_start
      t.string :place
      t.integer :ticket_price
      t.string :poster_image
      t.text :description

      t.timestamps
    end
  end
end
