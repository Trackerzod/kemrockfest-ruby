class SetDefaultToShowColumn < ActiveRecord::Migration[5.0]
  def change
    change_column :articles, :show, :boolean, default: true
    change_column :bands, :show, :boolean, default: true
    change_column :news, :show, :boolean, default: true
    change_column :rehearsal_points, :show, :boolean, default: true
  end
end
