class CreateRehearsalPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :rehearsal_points do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.string :address
      t.string :contacts
      t.string :price
      t.string :base_image
      t.boolean :show

      t.timestamps
    end
  end
end
