class AddReferenceToConcertAndPhotoAlbum < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :photo_albums, :concert, index: true
  end
end
