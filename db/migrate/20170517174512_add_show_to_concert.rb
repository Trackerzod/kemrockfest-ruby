class AddShowToConcert < ActiveRecord::Migration[5.0]
  def change
    add_column :concerts, :show, :boolean, default: true
  end
end
