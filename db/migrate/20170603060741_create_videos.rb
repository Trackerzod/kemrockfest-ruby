class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.string :name
      t.string :slug
      t.date :date_publication
      t.text :iframe_link
      t.string :preview_image
      t.text :description
      t.boolean :show

      t.timestamps
    end
  end
end
