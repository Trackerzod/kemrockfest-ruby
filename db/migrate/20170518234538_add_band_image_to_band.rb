class AddBandImageToBand < ActiveRecord::Migration[5.0]
  def change
    add_column :bands, :band_image, :string
  end
end
