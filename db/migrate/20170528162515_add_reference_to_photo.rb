class AddReferenceToPhoto < ActiveRecord::Migration[5.0]
  def change
    add_reference :photos, :photo_album, index: true
  end
end
