class CreateNews < ActiveRecord::Migration[5.0]
  def change
    create_table :news do |t|
      t.string :name
      t.string :slug
      t.date :date_publication
      t.text :preview_description
      t.text :description
      t.string :news_image
      t.boolean :show

      t.timestamps
    end
  end
end
