class RemoveDatePublicationFromPhotoAlbum < ActiveRecord::Migration[5.0]
  def change
    remove_column :photo_albums, :date_publication
  end
end
