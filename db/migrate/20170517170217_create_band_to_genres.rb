class CreateBandToGenres < ActiveRecord::Migration[5.0]
  def change
    create_table :band_to_genres do |t|
      t.references :band, index: true
      t.references :genre, index: true
      t.timestamps
    end
  end
end
