class CreateBandMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :band_members do |t|
      t.string :name
      t.string :music_role

      t.timestamps
    end
  end
end
