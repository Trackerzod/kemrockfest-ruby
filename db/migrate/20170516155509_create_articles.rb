class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :name
      t.string :slug
      t.date :date_publication
      t.string :article_image
      t.text :preview_description
      t.text :description
      t.boolean :show

      t.timestamps
    end
  end
end
