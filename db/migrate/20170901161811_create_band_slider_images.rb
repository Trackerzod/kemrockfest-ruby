class CreateBandSliderImages < ActiveRecord::Migration[5.0]
  def change
    create_table :band_slider_images do |t|
      t.string :image
      t.text :description
      t.belongs_to :band, index: true

      t.timestamps
    end
  end
end
