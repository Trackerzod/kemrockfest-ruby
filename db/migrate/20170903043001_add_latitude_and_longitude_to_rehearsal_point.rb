class AddLatitudeAndLongitudeToRehearsalPoint < ActiveRecord::Migration[5.0]
  def change
    add_column :rehearsal_points, :latitude, :float
    add_column :rehearsal_points, :longitude, :float
  end
end
