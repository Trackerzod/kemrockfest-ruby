# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2017_10_14_085428) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.date "date_publication"
    t.string "article_image"
    t.text "preview_description"
    t.text "description"
    t.boolean "show", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "band_members", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "music_role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "member_image"
  end

  create_table "band_slider_images", id: :serial, force: :cascade do |t|
    t.string "image"
    t.text "description"
    t.integer "band_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id"], name: "index_band_slider_images_on_band_id"
  end

  create_table "band_to_band_members", id: :serial, force: :cascade do |t|
    t.integer "band_id"
    t.integer "band_member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id"], name: "index_band_to_band_members_on_band_id"
    t.index ["band_member_id"], name: "index_band_to_band_members_on_band_member_id"
  end

  create_table "band_to_genres", id: :serial, force: :cascade do |t|
    t.integer "band_id"
    t.integer "genre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id"], name: "index_band_to_genres_on_band_id"
    t.index ["genre_id"], name: "index_band_to_genres_on_genre_id"
  end

  create_table "bands", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.date "date_based"
    t.text "description"
    t.boolean "show", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "band_image"
  end

  create_table "ckeditor_assets", id: :serial, force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "concert_to_bands", id: :serial, force: :cascade do |t|
    t.integer "concert_id"
    t.integer "band_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id"], name: "index_concert_to_bands_on_band_id"
    t.index ["concert_id"], name: "index_concert_to_bands_on_concert_id"
  end

  create_table "concerts", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "date_start"
    t.string "place"
    t.integer "ticket_price"
    t.string "poster_image"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "show", default: true
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "genres", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.date "date_publication"
    t.text "preview_description"
    t.text "description"
    t.string "news_image"
    t.boolean "show", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "photo_albums", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "description"
    t.string "preview_image"
    t.boolean "show", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "concert_id"
    t.index ["concert_id"], name: "index_photo_albums_on_concert_id"
  end

  create_table "photos", id: :serial, force: :cascade do |t|
    t.string "photo"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "photo_album_id"
    t.index ["photo_album_id"], name: "index_photos_on_photo_album_id"
  end

  create_table "rehearsal_point_slider_images", id: :serial, force: :cascade do |t|
    t.string "image"
    t.text "description"
    t.integer "rehearsal_point_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rehearsal_point_id"], name: "index_rehearsal_point_slider_images_on_rehearsal_point_id"
  end

  create_table "rehearsal_points", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "description"
    t.string "address"
    t.string "contacts"
    t.string "price"
    t.string "base_image"
    t.boolean "show", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
  end

  create_table "subscribers", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "videos", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.date "date_publication"
    t.text "iframe_link"
    t.string "preview_image"
    t.text "description"
    t.boolean "show"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
