# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#** Delete all data
User.delete_all
News.delete_all
Article.delete_all
RehearsalPoint.delete_all
Concert.delete_all
Band.delete_all
BandMember.delete_all
Genre.delete_all
BandToBandMember.delete_all
ConcertToBand.delete_all
BandToGenre.delete_all
PhotoAlbum.delete_all
Photo.delete_all
Video.delete_all

music_roles = [:guitar, :bass, :drums, :keys, :vocal, :back_vocal, :dj]

#** Seed admin user
User.create(email: 'admin@kemrockfest.ru', password: '123456789')

#** Seed articles data
10.times do |i|
  Article.create!(
      name: 'Water, Please поделились секретом своего звука',
      slug: "article-#{i}",
      date_publication: Date.today + i.day,
      article_image: File.new("#{Rails.public_path}/dev_pics/dev_new.jpg"),
      preview_description: Faker::Lorem.paragraph,
      description: Faker::Lorem.paragraph,
      show: true
  )
end

#** Seed news data
10.times do |i|
  News.create!(
      name: 'Архон распались! Состоится прощальный концерт.',
      slug: "news-#{i}",
      date_publication: Date.today + i.day,
      preview_description: Faker::Lorem.paragraph,
      description: Faker::Lorem.paragraph,
      news_image: File.new("#{Rails.public_path}/dev_pics/dev_new.jpg"),
      show: true
  )
end

#** Seed rehearsal_point data
10.times do |i|
  rehearsal_point = RehearsalPoint.new(
      name: 'Mordor Music Hall',
      slug: "rep-base-#{i}",
      description: Faker::Lorem.paragraph,
      address: Faker::Address.street_address,
      latitude: 55.36523588,
      longitude: 86.0618233,
      contacts: "#{Faker::PhoneNumber.cell_phone}, с 10:00 до 20:00",
      price: '500р. - 2 часа',
      base_image: File.new("#{Rails.public_path}/dev_pics/dev_pic.jpg"),
      show: true
  )

  5.times do
    rehearsal_point.rehearsal_point_slider_images << RehearsalPointSliderImage.create!(
                        image: File.new("#{Rails.public_path}/dev_pics/dev_pic.jpg"),
                        description: "Some description"
    )
  end

  rehearsal_point.save!
end

#** Seed genre data
Genre.create!(name: 'Trash Metal')
Genre.create!(name: 'Doom Metal')
Genre.create!(name: 'Melodic Metal')
Genre.create!(name: 'Hard Rock')
Genre.create!(name: 'Djent')
Genre.create!(name: 'Punk')
Genre.create!(name: 'Instrumental')
Genre.create!(name: 'Industrial')
Genre.create!(name: 'Alternative')
Genre.create!(name: 'Pop')
Genre.create!(name: 'Trance')

#** Seed band_member data
25.times do
  BandMember.create!(
      name: Faker::Name.name,
      music_role: music_roles[rand(0..music_roles.count-1)],
      member_image: File.new("#{Rails.public_path}/dev_pics/James-Hetfield-happy.jpg")
  )
end

#** Seed bands data
band_member_ids = BandMember.ids
genre_ids = Genre.ids

10.times do |i|
  band = Band.new(
      name: Faker::RockBand.name,
      slug: "band-#{i}",
      date_based: Date.today + i.day,
      description: Faker::Hipster.paragraph,
      show: true,
      band_image: File.new("#{Rails.public_path}/dev_pics/metallica-band.jpg"),
  )
  band.band_members << BandMember.find(band_member_ids[rand(0..band_member_ids.count - 1)])
  band.band_members << BandMember.find(band_member_ids[rand(0..band_member_ids.count - 1)])
  band.band_members << BandMember.find(band_member_ids[rand(0..band_member_ids.count - 1)])
  band.band_members << BandMember.find(band_member_ids[rand(0..band_member_ids.count - 1)])

  5.times do
    band.band_slider_images << BandSliderImage.create!(
        image: File.new("#{Rails.public_path}/dev_pics/dev_pic.jpg"),
        description: "Some description"
    )
  end

  rand_count_genres = rand(1..genre_ids.count)
  rand_count_genres.times do
    band.genres << Genre.find(genre_ids[rand(0..genre_ids.count - 1)])
  end

  band.save!
end

#** Seed concerts data
band_ids = Band.ids

10.times do |i|
  concert = Concert.new(
      name: Faker::Company.name,
      slug: "concet-#{i}",
      date_start: DateTime.now + i.month,
      place: Faker::App.name,
      ticket_price: 200,
      poster_image: File.new("#{Rails.public_path}/dev_pics/next-fest-big.png"),
      description: Faker::Hipster.paragraph,
      show: true
  )
  rand_count_bands = rand(1..band_ids.count)
  rand_count_bands.times do
    concert.bands << Band.find(band_ids[rand(0..band_ids.count - 1)])
  end

  album = PhotoAlbum.new(
      name: 'Old New Year Fest',
      slug: "photo-album-#{i}",
      preview_image: File.new("#{Rails.public_path}/dev_pics/dev_new.jpg"),
      description: Faker::Lorem.paragraph,
      show: true
  )
  10.times do |k|
    photo = Photo.create!(
        photo: File.new("#{Rails.public_path}/dev_pics/dev_new.jpg"),
        comment: "photo-#{k}"
    )
    album.photos << photo
  end
  album.save!
  concert.photo_album = album

  concert.save!
end

10.times do |i|
  Video.create(
           name: Faker::Artist.name,
           slug: "video-#{i}",
           date_publication: Date.today + i.day,
           iframe_link: '<iframe width="853" height="480" src="https://www.youtube.com/embed/OhE8XqoHJ5g" frameborder="0" allowfullscreen></iframe>',
           preview_image: File.new("#{Rails.public_path}/dev_pics/header-dissimulator-publicityphoto.jpg"),
           description: Faker::Lorem.paragraph,
           show: true
  )
end