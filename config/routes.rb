# == Route Map
#

Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/', controller: 'home', action: 'index'

  get '/concerts', controller: 'concert', action: 'index', as: 'concert_index'
  get '/concerts/:slug', controller: 'concert', action: 'show', as: 'concert_show'
  get '/future-concerts', controller: 'concert', action: 'future_concerts', as: 'future_concerts_index'
  get '/archive-concerts', controller: 'concert', action: 'archive_concerts', as: 'archive_concerts_index'

  get '/news', controller: 'news', action: 'index', as: 'news_index'
  get '/news/:slug', controller: 'news', action: 'show', as: 'news'

  get '/articles', controller: 'article', action: 'index', as: 'article_index'
  get '/articles/:slug', controller: 'article', action: 'show', as: 'article_show'

  get '/bands', controller: 'band', action: 'index', as: 'band_index'
  get '/bands/:slug', controller: 'band', action: 'show', as: 'band_show'

  get '/bases', controller: 'base', action: 'index', as: 'base_index'
  get '/bases/:slug', controller: 'base', action: 'show', as: 'base_show'

  get '/gallery', controller: 'photo_album', action: 'index', as: 'gallery_index'
  get '/gallery/:slug', controller: 'photo_album', action: 'show', as: 'gallery_show'

  get '/videos', controller: 'video', action: 'index', as: 'video_index'
  get '/videos/:slug', controller: 'video', action: 'show', as: 'video_show'

  post '/subscribers', to: 'subscriber#create', as: 'create_subscriber'
end
