require_relative 'boot'

require 'rails/all'

require 'carrierwave'
require 'mini_magick'
# require "paperclip/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Kemrockfest
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
      config.i18n.load_path += Dir[Rails.root.join('lib', 'locale', '*.{rb,yml}')]
      # Белый список локалей, доступных приложению
      config.i18n.available_locales = [:en, :ru]
      config.i18n.default_locale = :ru
      config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
      config.eager_load_paths << Rails.root.join('lib/unisender')
      config.time_zone = "Asia/Krasnoyarsk"
      config.active_record.default_timezone = :local
      config.assets.paths << Rails.root.join("vendor", "assets", "images")
      config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif *.svg)
      config.active_job.queue_adapter = :sidekiq
  end
end
