RailsAdmin.config do |config|
  require 'i18n'
  I18n.default_locale = :ru

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)
  config.excluded_models = ["User",
                            "Ckeditor::Asset",
                            "Ckeditor::AttachmentFile",
                            "Ckeditor::Picture"]

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.actions do
    dashboard do
      except %w(Photo BandSliderImage RehearsalPointSliderImage)
    end
    index do
      except %w(Photo BandToBandMember BandToGenre ConcertToBand BandSliderImage RehearsalPointSliderImage)
    end
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'News' do
    navigation_label 'Пресса'

    list do
      include_fields :name, :date_publication, :slug, :news_image
    end

    edit do
      configure :description, :ck_editor
      configure :preview_description, :ck_editor
      include_fields :name, :date_publication, :preview_description, :description, :news_image, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      configure :preview_description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :date_publication, :preview_description, :description, :news_image, :show
    end
  end

  config.model 'Article' do
    navigation_label 'Пресса'

    list do
      include_fields :name, :date_publication, :slug, :article_image
    end

    edit do
      configure :description, :ck_editor
      configure :preview_description, :ck_editor
      include_fields :name, :date_publication, :preview_description, :description, :article_image, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      configure :preview_description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :date_publication, :preview_description, :description, :article_image, :show
    end
  end

  config.model 'RehearsalPoint' do
    navigation_label 'Реп-точки'

    list do
      include_fields :name, :address, :contacts, :price
    end

    edit do
      configure :description, :ck_editor
      include_fields :name, :description, :address, :latitude, :longitude,
                     :contacts, :price, :base_image, :rehearsal_point_slider_images,:show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :description, :address, :latitude, :longitude,
                     :contacts, :price, :base_image, :rehearsal_point_slider_images, :show
    end
  end

  config.model 'RehearsalPointSliderImage' do
    navigation_label 'Реп-точки'

    edit do
      include_fields :image, :description
    end

    show do
      include_fields :image, :description
    end
  end

  config.model 'Band' do
    navigation_label 'Группы'

    list do
      include_fields :name, :slug, :date_based
    end

    edit do
      configure :description, :ck_editor
      include_fields :name, :date_based, :band_image, :band_members, :genres, :band_slider_images, :description, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :date_based, :band_image, :band_slider_images, :description, :show
    end
  end

  config.model 'BandSliderImage' do
    navigation_label 'Группы'

    edit do
      include_fields :image, :description
    end

    show do
      include_fields :image, :description
    end
  end

  config.model 'BandMember' do
    navigation_label 'Группы'

    list do
      include_fields :name, :music_role
    end

    edit do
      include_fields :name, :music_role, :member_image
    end

    show do
      include_fields :name, :music_role, :member_image
    end
  end

  config.model 'Genre' do
    navigation_label 'Группы'

    list do
      include_fields :name
    end

    edit do
      include_fields :name
    end

    show do
      include_fields :name
    end
  end

  config.model 'Concert' do
    navigation_label 'Концерты'

    list do
      include_fields :name, :date_start, :place, :ticket_price, :slug
    end

    edit do
      configure :description, :ck_editor
      include_fields :name, :date_start, :place, :bands, :ticket_price, :poster_image, :description, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :date_start, :place, :ticket_price, :poster_image, :description, :show
    end
  end

  config.model 'PhotoAlbum' do
    navigation_label 'Пресса'

    list do
      include_fields :name, :slug, :concert, :preview_image
    end

    edit do
      configure :description, :ck_editor
      include_fields :name, :concert, :photos, :preview_image, :description, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :concert, :preview_image, :description, :show
    end
  end

  config.model 'Photo' do
    list do
      exclude_fields :photo_album
    end

    edit do
      exclude_fields :photo_album
    end

    show do
      exclude_fields :photo_album
    end
  end

  config.model 'Video' do
    navigation_label 'Пресса'
    list do
      include_fields :name, :date_publication, :preview_image
    end

    edit do
      configure :description, :ck_editor
      include_fields :name, :date_publication, :iframe_link, :preview_image, :description, :show
    end

    show do
      configure :description do
        pretty_value { value.try :html_safe }
      end
      include_fields :name, :slug, :date_publication, :iframe_link, :preview_image, :description, :show
    end
  end
end
