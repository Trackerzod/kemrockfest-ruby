# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'kemrockfest'
set :repo_url, 'git@bitbucket.org:Trackerzod/kemrockfest-ruby.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/var/www/rails/#{fetch :application}"
set :use_sudo, false
set :rvm_ruby_version, '2.3.2'
set :rails_env, 'production'

# Default value for :scm is :git
set :scm, :git

# Assets compiling proceed via Rsync.
set :deploy_via, :remote_cache

set :rsync_options, %w[--recursive --delete --delete-excluded --exclude .git*]
set :rsync_cache, nil

set :rsync_stage, "../.capistrano_deploy/#{fetch(:application)}"

# Default value for :linked_files is []
#set :linked_files, fetch(:linked_files, []).push('config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads', 'public/ckeditor_assets')

task :precompile do
  Dir.chdir fetch(:rsync_stage) do
    system 'rake', 'assets:precompile'
  end
end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    file_path = File.join(current_path,'tmp','restart.txt')
    on roles(:app) do
      execute :touch, file_path
    end
    invoke 'puma:stop'
    invoke 'puma:start'
    invoke 'sidekiq:stop'
    invoke 'sidekiq:start'
  end
end

after 'rsync:stage', 'precompile'
after 'deploy:finished', 'deploy:restart'