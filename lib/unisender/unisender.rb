require 'net/http'

module Unisender
  API_KEY = Rails.configuration.unisender_api_key
  SENDER_NAME = "Kemrockfest.ru"
  SENDER_EMAIL = "info.kemrockfest@yandex.ru"
  MAIL_LIST_ID = "11054233"

  def self.add_contact_to_list(email, double_optin=3)
    params = {
        api_key: API_KEY,
        list_ids: MAIL_LIST_ID,
        "fields[email]": email,
        double_optin: double_optin
    }
    uri = 'https://api.unisender.com/ru/api/subscribe?format=json'

    logger = Logger.new(Rails.root.join('log', 'unisender.log'))
    logger.info "Try subscribe #{email} to unisender"

    response = Net::HTTP.post_form(URI.parse(uri), params)
    response_data = JSON.parse(response.body)

    if response.code == '200' && (result = response_data['result'])
      logger.info "Subscriber #{email} successful added in list. Subscriber id = #{result["person_id"]}"

    elsif !(response_data.is_a? Array) && response_data['error'] && (error_code = response_data['code'])
      logger.error "Error subscribe method. Error code: #{error_code}"
    end

  end

  def self.remove_contact_from_list(email, contact_type="email")
    params = {
        api_key: API_KEY,
        contact_type: contact_type,
        contact: email,
        list_ids: MAIL_LIST_ID
    }

    uri = 'https://api.unisender.com/ru/api/exclude?format=json'

    logger = Logger.new(Rails.root.join('log', 'unisender.log'))
    logger.info "Try to exclude #{email} from unisender"

    response = Net::HTTP.post_form(URI.parse(uri), params)
    response_data = JSON.parse(response.body)

    if response.code == '200' && response_data["result"]
      logger.info "Subscriber #{email} successful excluded from list"
    elsif !(response_data.is_a? Array) && response_data['error'] && (error_code = response_data['code'])
      logger.error "Error exclude method. Error code #{error_code}"
    end

  end

  def self.create_email_message(subject, body)
    params = {
        api_key: API_KEY,
        sender_name: SENDER_NAME,
        sender_email: SENDER_EMAIL,
        subject: subject,
        body: body,
        list_id: MAIL_LIST_ID
    }

    uri = 'https://api.unisender.com/ru/api/createEmailMessage?format=json'

    logger = Logger.new(Rails.root.join('log', 'unisender.log'))
    logger.info "Try to create email message in unisender"

    response = Net::HTTP.post_form(URI.parse(uri), params)
    response_data = JSON.parse(response.body)

    if !response_data["error"] && (result = response_data["result"])
      logger.info "Email message created successful. Message id = #{result["message_id"]}"
      Unisender.send_email_message(result["message_id"])

    elsif !(response_data.is_a? Array) && response_data['error'] && (error_code = response_data['code'])
      logger.info "Error when creating message. Error = #{response_data['error']}. Error code = #{error_code}"
    end
  end

  def self.send_email_message(message_id)
    params = {
        api_key: API_KEY,
        message_id: message_id
    }

    uri = 'https://api.unisender.com/ru/api/createCampaign?format=json'

    logger = Logger.new(Rails.root.join('log', 'unisender.log'))
    logger.info "Try to send newsletter command in unisender"

    response = Net::HTTP.post_form(URI.parse(uri), params)
    response_data = JSON.parse(response.body)

    if !response_data["error"] && (result = response_data["result"])
      logger.info "Campaign created successful. Campaign id = #{result["campaign_id"]}. Status = #{result["status"]}. Count = #{result["count"]}"

    elsif !(response_data.is_a? Array) && response_data['error'] && (error_code = response_data['code'])
      logger.info "Error when creating campaign. Error = #{response_data['error']}. Error code = #{error_code}"
    end
  end

end