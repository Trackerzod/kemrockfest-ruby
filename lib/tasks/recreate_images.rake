namespace :recreate_images do

  desc 'Recreate all images'
  task all: :environment do
    puts 'Starting recreate all images'
    Rake::Task['recreate_images:articles'].invoke
    Rake::Task['recreate_images:band_members'].invoke
    Rake::Task['recreate_images:bands'].invoke
    Rake::Task['recreate_images:concerts'].invoke
    Rake::Task['recreate_images:news'].invoke
    Rake::Task['recreate_images:rehearsal_poins'].invoke
    puts 'All Done!'
  end

  desc 'Recreate articles images'
  task articles: :environment do
    puts 'Starting recreate images of Article model'
    articles = Article.all
    articles.each do |article|
      article.article_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate band_members images'
  task band_members: :environment do
    puts 'Starting recreate images of BandMember model'
    band_members = BandMember.all
    band_members.each do |band_member|
      band_member.member_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate bands images'
  task bands: :environment do
    puts 'Starting recreate images of Band model'
    bands = Band.all
    bands.each do |band|
      band.band_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate concerts images'
  task concerts: :environment do
    puts 'Starting recreate images of Concert model'
    concerts = Concert.all
    concerts.each do |concert|
      concert.poster_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate news images'
  task news: :environment do
    puts 'Starting recreate images of News model'
    news = News.all
    news.each do |new|
      new.news_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate rehearsal_poins images'
  task rehearsal_poins: :environment do
    puts 'Starting recreate images of RehearsalPoint model'
    rehearsal_poins = RehearsalPoint.all
    rehearsal_poins.each do |rehearsal_poin|
      rehearsal_poin.base_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate photo_albums images'
  task photo_albums: :environment do
    puts 'Starting recreate images of PhotoAlbum model'
    photo_albums = PhotoAlbum.all
    photo_albums.each do |album|
      album.preview_image.recreate_versions!
    end
    puts 'Done!'
  end

  desc 'Recreate photos images'
  task photos: :environment do
    puts 'Starting recreate images of Photo model'
    photos = Photo.all
    photos.each do |photo|
      photo.photo.recreate_versions!
    end
    puts 'Done!'
  end

end