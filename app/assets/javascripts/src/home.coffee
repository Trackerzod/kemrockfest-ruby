# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load": ->
  new window.TimeCounter()

  parPosition = []

  $('.par').each ->
    parPosition.push $(this).offset().top
    return

  $(document).on 'scroll', ->
    position = $(document).scrollTop()
    index = undefined
    i = 0
    while i < parPosition.length
      if position <= parPosition[i]
        index = i
        break
      i++
    $('.nav-dot').removeClass('active').eq(index).addClass 'active'
    return

  $('a').click ->
    if $(this).hasClass("nav-dot")
      event.preventDefault();
      id  = $(this).attr('href')
      top = $(id).offset().top;
      $('body,html').animate({scrollTop: top-200}, 1000);
    $('.nav-dot').removeClass('active')
    $(this).addClass('active')