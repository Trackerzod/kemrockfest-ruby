class window.BandSlider
  constructor: ->
    @sliderContainer = $('#band-slider');
    @sliderPrevArrow = "<button class='base-detail-page__slider-arrow base-detail-page__slider-arrow--prev'></button>"
    @sliderNextArrow = "<button class='base-detail-page__slider-arrow base-detail-page__slider-arrow--next'></button>"

    @init()

  init: =>
    self = this
    @sliderContainer.slick(
      slidesToShow: 4,
      slidesToScroll: 1,
      adaptiveHeight: true,
      prevArrow: self.sliderPrevArrow,
      nextArrow: self.sliderNextArrow
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    )

    new window.AlbumClose()