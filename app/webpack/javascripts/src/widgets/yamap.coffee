class window.YaMap
  constructor: (selector_id) ->
    self = this
    @mapSelector = selector_id
    @latitude = parseFloat($('#' + selector_id).attr('data-latitude'))
    @longitude = parseFloat($('#' + selector_id).attr('data-longitude'))
    @hint = $('#' + selector_id).attr('data-hint')
    @content = $('#' + selector_id).attr('data-content')
    @yaMap = undefined
    @yMaps = ymaps
    @init()

  init: =>
    self = this

    if $('#'+@mapSelector)[0] != undefined
      @yMaps.ready ->
        self.yaMap = new self.yMaps.Map(self.mapSelector, {
          center: [self.latitude, self.longitude],
          zoom: 16
        })

        myPlacemark = new self.yMaps.Placemark([self.latitude, self.longitude],
          { hintContent: self.hint, balloonContent: '<p>'+self.hint+'</p><p>'+self.content+'</p>' });

        self.yaMap.geoObjects.add(myPlacemark);