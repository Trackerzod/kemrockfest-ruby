class window.TimeCounter
  constructor: ->
    self = this
    @lastConcertTime = $('#last_concert_time').attr('data-last-concert-time')
    @concertSeconds = $('#concert_seconds')
    @concertMinutes = $('#concert_minutes')
    @concertMinutesTitle = $('#concert_minutes_title')
    @concertHours = $('#concert_hours')
    @concertHoursTitle = $('#concert_hours_title')
    @concertDays = $('#concert_days')
    @concertDaysTitle = $('#concert_days_title')

    @init()

  init: ->
    self = this
    setInterval(@concert_timer, 1000, @lastConcertTime)

  concert_timer: (target_time) =>
    current_time = new Date().getTime();
    seconds = Math.floor((target_time - current_time) / 1000)
    days = Math.floor(seconds/24/60/60)
    hours_left = Math.floor((seconds) - (days*86400))
    hours = Math.floor(hours_left / 3600)
    minutesLeft = Math.floor((hours_left) - (hours*3600))
    minutes = Math.floor(minutesLeft / 60)
    remainingSeconds = seconds % 60
    @concertSeconds.html(@zero_before_number(remainingSeconds))
    @concertMinutes.html(@zero_before_number(minutes))
    @concertMinutesTitle.html(@localize_minutes(minutes))
    @concertHours.html(@zero_before_number(hours))
    @concertHoursTitle.html(@localize_hours(hours))
    @concertDays.html(@zero_before_number(days))
    @concertDaysTitle.html(@localize_days(days))

  zero_before_number: (number) ->
    if number < 10
      return "0" + number
    else
      return number

  localize_minutes: (time_number) ->
    minute_string = "Минут"
    time_string = time_number.toString()
    last_number = parseInt(time_string.charAt(time_string.length - 1))

    if last_number == 1
      return minute_string = "Минута"
    if last_number == 0 || last_number > 4 || (time_string > 10 && time_string < 21)
      return minute_string = "Минут"
    else
      return minute_string = "Минуты"

  localize_hours: (time_number) ->
    hour_string = "Минут"
    time_string = time_number.toString()
    last_number = parseInt(time_string.charAt(time_string.length - 1))

    if last_number == 1
      return hour_string = "Час"
    if last_number == 0 || last_number > 4 || (time_string > 10 && time_string < 21)
      return hour_string = "Часов"
    else
      return hour_string = "Часа"

  localize_days: (time_number) ->
    day_string = "Минут"
    time_string = time_number.toString()
    last_number = parseInt(time_string.charAt(time_string.length - 1))

    if last_number == 1
      return day_string = "День"
    if last_number == 0 || last_number > 4 || (time_string > 10 && time_string < 21)
      return day_string = "Дней"
    else
      return day_string = "Дня"