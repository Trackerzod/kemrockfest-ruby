class window.BaseForm
  constructor: ->
    @form = $('.js-form')
    @formErrorsBlock = undefined
    @formTitle = undefined

    @init()

  init: =>
    self = this

    @form.on "ajax:success", (e, data, status, xhr) ->
      self.formTitle = self.form.find('.js-form-title')
      self.formErrorsBlock = self.form.find('.js-form-errors-block')
      self.remove_errors()
      self.form_success(data, status, xhr)

    @form.on "ajax:error", (e, xhr, status, error) ->
      self.formTitle = self.form.find('.js-form-title')
      self.formErrorsBlock = self.form.find('.js-form-errors-block')
      self.remove_errors()
      self.form_error(xhr)

  form_success: (data, status, xhr) =>
    self = this
    if data.status == "subscribe"
      self.formTitle.html("Спасибо за подписку!")
    else
      self.formTitle.html("Вы успешно отписались.")

  form_error: (data) =>
    self = this

    errors = data.responseJSON
    $.each errors, (index, field) ->
      $.each field, (key, value) ->
        error_label = "<label>" + value + "</label>"
        $(self.formErrorsBlock).append(error_label)

  remove_errors: =>
    self = this
    @formTitle.html("Подписаться на рассылку")
    @formErrorsBlock.html("")