class window.AlbumClose
  constructor:->
    @mobileMenuButton = $('.mobile-menu-button')
    @photoAlbumLink = $('.js-photo-open')

    @init()

  init: =>
    self = this

    @photoAlbumLink.click ->
      self.mobileMenuButton.addClass("active")