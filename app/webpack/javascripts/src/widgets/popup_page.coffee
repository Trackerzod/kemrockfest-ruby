class window.PopupPage
  constructor: ->
    @body = $('body')
    @popupContainer = $('.popup-page-container')
    @popupPageCloseButton = $('.popup-page__close-button')
    @popupWrapper = $('.popup-page-wrapper')
    @popupMask = $('.popup-mask')
    @popupLink = $('.js-popup-link')
    @currentLink = window.location.href;
    @popupOpen = false

    @init()

  init: =>
    self = this

    @closePopup()

    @popupLink.click (e) ->
      e.preventDefault()
      popup_link = this

      unless self.popupOpen
        self.popupOpen = true
        url = $(popup_link).attr('href');
        $.ajax
          url: url
          dataType: 'html'
          method: 'GET'
          success: (data) ->
            window.history.pushState({turbolinks: true, url: url}, '', url)
            popupPageContainer = $(data).find('.js-page-container').html()
            self.popupContainer.append(popupPageContainer)

            if $(data).find('.band-detail-wrapper') != undefined
              # принудительная задержка для инициализации Slick
              setTimeout (->
                self.initBandSlider()
              ), 100

            if $(data).find('.base-detail-page__map') != undefined
              maps = $(data).find('.base-detail-page__map')
              for map in maps
                self.initYMap($(map).attr('id'))
            new window.VKShare()
            self.activePopup()

  activePopup: =>
    @popupWrapper.addClass('active');
    @popupMask.addClass('active');
    @body.addClass('no-scrollable');

  closePopup: =>
    self = this

    @popupPageCloseButton.click ->
      self.popupWrapper.removeClass('active');
      self.popupMask.removeClass('active');
      self.body.removeClass('no-scrollable');
      self.popupContainer.html('')
      window.history.pushState({turbolinks: true, url: self.currentLink}, '', self.currentLink)
      self.popupOpen = false

  initBandSlider: =>
    new window.BandSlider()

  initYMap: (selector_id) =>
    new window.YaMap(selector_id)