class window.AjaxPageLoader
  constructor: (elementsContainer) ->
    self = this
    @loadMore = true
    @loadButton = $('.show-more')
    @elementsContainer = $('.js-ajax-load');
    @inProcess = false

    @init()

  init: =>
    self = this
    @loadButton.click (e) ->
      e.preventDefault()
      unless self.inProcess
        self.inProcess = true
        if self.loadMore
          self.loadMore = false
          $.ajax({
            url: self.loadButton.attr('href')
            dataType: 'html'
            success: (data, status, xhr) ->
              self.loadSuccess(data, status, xhr)
            error: (data) ->
              self.loadError(data)
          })

  loadSuccess: (data, status, xhr) =>
    self = this
    nextLink = $('.show-more').attr('href')

    window.history.pushState({turbolinks: true, url: nextLink}, '', nextLink)
    setTimeout (->
      $('.show-more').remove()
      self.elementsContainer.find('.clearfix').remove()
      self.initLoadedContent(data)
      return
    ), 500
    @inProcess = false

  loadError: (data) ->

  initLoadedContent: (html) =>
    self = this

    @loadMore = true
    loadedElements = $(html).find('.js-ajax-load').html()
    self.elementsContainer.append(loadedElements)
    @loadButton = $('.show-more')
    new window.PopupPage()

    @init()