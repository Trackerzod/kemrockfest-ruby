$(document).on 'turbolinks:load': ->
  $('.mobile-menu-button').click ->
    mobile_menu_button = $(this);
    header_mobile_popup = $('.header-mobile-popup')
    popup_height = document.body.offsetHeight

    if mobile_menu_button.hasClass('active') == true
      mobile_menu_button.removeClass('active')
      header_mobile_popup.removeClass('active')
      $('body').removeClass('no-scrollable')
      header_mobile_popup.css('height', '0px')
    else
      mobile_menu_button.addClass('active')
      header_mobile_popup.addClass('active')
      $('body').addClass('no-scrollable')
      header_mobile_popup.css('height', popup_height+'px')