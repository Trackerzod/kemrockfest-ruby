if (window.CKEDITOR) {
    CKEDITOR.editorConfig = function (config) {
        config.allowedContent = true;
        config.filebrowserBrowseUrl = "/ckeditor/attachment_files";
//    config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";
//    config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";
        config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";
        config.filebrowserImageBrowseUrl = "/ckeditor/pictures";
        config.filebrowserImageUploadUrl = "/ckeditor/pictures";
        config.filebrowserUploadUrl = "/ckeditor/attachment_files";

        config.language = 'ru';
        config.toolbar_lessBar = [
            { name: 'document', items: ['Source'] },
            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },
            { name: 'editing', items: [ 'Find', 'Replace',  'SelectAll' ] },
            { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList',  'Outdent', 'Indent',
                'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            '/',
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike',  'RemoveFormat' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'styles', items: [ 'Format', 'FontSize' ] },
            { name: 'links', items: [ 'Link', 'Unlink' ] },
            { name: 'tools', items: [ 'Maximize',  'About' ] }
        ];
    };
}