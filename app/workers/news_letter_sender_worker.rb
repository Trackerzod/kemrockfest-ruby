class NewsLetterSenderWorker
  include Sidekiq::Worker

  def perform(class_name, id)
    class_name.constantize.create_newsletter(id)
  end
end
