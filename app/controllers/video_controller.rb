class VideoController < ApplicationController
  include ViewHelper

  def index
    videos = Video.all.order(date_publication: :desc)
    @videos = Kaminari.paginate_array(split_data(videos, 3)).page(params[:page]).per(3)
  end

  def show
    @video = Video.find_by_slug params[:slug]

    respond_to do |format|
      format.html
      format.js
      format.json { render json: @video }
    end
  end
end
