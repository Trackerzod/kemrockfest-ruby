class SubscriberController < ApplicationController
  def create
    new_subscriber = Subscriber.new(subscriber_params)
    subscriber = Subscriber.find_by_email(new_subscriber.email)

    if subscriber.nil?
      save_subscriber(new_subscriber)
    else
      if subscriber.subscribed?
        subscriber.status = :unsubscribe
      else
        subscriber.status = :subscribe
      end
      save_subscriber(subscriber)
    end
  end

  private
  def save_subscriber(subscriber)
    respond_to do |format|
      if subscriber.save
        format.json { render json: subscriber, status: :created }
      else
        format.json { render json: subscriber.errors, status: :unprocessable_entity }
      end
    end
  end

  def subscriber_params
    params.require(:subscriber).permit(:email)
  end
end
