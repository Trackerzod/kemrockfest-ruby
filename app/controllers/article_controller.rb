class ArticleController < ApplicationController
  def index
    articles = Article.where(show: true).order(date_publication: :desc)
    @articles = Kaminari.paginate_array(articles).page(params[:page]).per(5)
  end

  def show
    @article = Article.find_by_slug(params[:slug])

    respond_to do |format|
      format.html
      format.js
      format.json { render json: @article }
    end
  end
end
