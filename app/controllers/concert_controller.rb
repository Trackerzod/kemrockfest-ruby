class ConcertController < ApplicationController
  include ViewHelper

  def index
    @concerts = get_future_concerts(5)
    @archive_concerts = get_archive_concerts(4)
  end

  def show
    @concert = Concert.find_by_slug(params[:slug])
  end

  def future_concerts
    @future_concerts = Kaminari.paginate_array(get_future_concerts).page(params[:page]).per(8)
  end

  def archive_concerts
    @archive_concerts = Kaminari.paginate_array(get_archive_concerts).page(params[:page]).per(8)
  end

  private
  def get_future_concerts(first_count=nil)
    if first_count.nil?
      Concert.where('date_start >= now()').includes(:bands).order(:date_start)
    else
      Concert.where('date_start >= now()').includes(:bands).order(:date_start).first(first_count)
    end
  end

  def get_archive_concerts(first_count=nil)
    if first_count.nil?
      Concert.where('date_start < now()').includes(:bands).order(:date_start)
    else
      Concert.where('date_start < now()').includes(:bands).order(:date_start).first(4)
    end
  end

end
