class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :new_subscriber

  private
  def new_subscriber
    @subscriber = Subscriber.new
  end
end
