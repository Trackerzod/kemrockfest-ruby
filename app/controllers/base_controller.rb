class BaseController < ApplicationController
  def index
    bases = RehearsalPoint.order(:name)
    @bases = Kaminari.paginate_array(bases).page(params[:page]).per(5)
  end

  def show
    @base = RehearsalPoint.find_by_slug(params[:slug])
  end
end
