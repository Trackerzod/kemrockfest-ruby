class BandController < ApplicationController
  def index
    bands = Band.includes(:band_members, :genres).order(:name)
    bands = bands.where("genres.name like '#{params[:genre]}'").references(:genres).order(:name) unless params[:genre].nil?
    @bands = Kaminari.paginate_array(bands).page(params[:page]).per(5)
  end

  def show
    @band = Band.find_by_slug(params[:slug])
  end

  def genre
    @bands = Band.includes(:band_members).where(name: params[:genre]).order(:name)
  end
end
