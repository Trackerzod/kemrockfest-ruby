class PhotoAlbumController < ApplicationController
  include ViewHelper
  def index
    concerts = Concert.includes(:photo_album).where.not(photo_albums: {id: nil}).where('date_start <= now()').order(date_start: :desc)
    @concerts = Kaminari.paginate_array(concerts).page(params[:page]).per(8)
  end

  def show
    @album = PhotoAlbum.find_by_slug(params[:slug])
    @photos = @album.photos
  end
end
