require 'date'

class HomeController < ApplicationController
  def index
    @concerts = Concert.includes(:bands).includes(:photo_album).where("date_start >= now()").order(:date_start).first(7)
    @news = News.order(date_publication: :desc).first(6)
    @photo_albums = Concert.includes(:photo_album).where.not(:photo_albums => { id: nil }).where("date_start >= now()").order(:date_start).first(5)
    @last_concert_time = get_milliseconds @concerts[0].date_start unless @concerts[0].nil?
    @videos = Video.order(date_publication: :desc).first(5)
  end

  private
    def get_milliseconds(date)
      (date.to_f * 1000).to_i
    end
end
