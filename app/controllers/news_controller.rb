class NewsController < ApplicationController
  def index
    news = News.where(show: true).order(date_publication: :desc)
    @news = Kaminari.paginate_array(news).page(params[:page]).per(5)
  end

  def show
    @new = News.find_by_slug(params[:slug])

    respond_to do |format|
      format.html
      format.js
      format.json { render json: @new }
    end
  end
end
