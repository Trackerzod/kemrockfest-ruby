# == Schema Information
#
# Table name: band_to_band_members
#
#  id             :integer          not null, primary key
#  band_id        :integer
#  band_member_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_band_to_band_members_on_band_id         (band_id)
#  index_band_to_band_members_on_band_member_id  (band_member_id)
#

class BandToBandMember < ApplicationRecord

  self.table_name = 'band_to_band_members'

  belongs_to :band, foreign_key: :band_id
  belongs_to :band_member, foreign_key: :band_member_id
end
