# == Schema Information
#
# Table name: news
#
#  id                  :integer          not null, primary key
#  name                :string
#  slug                :string
#  date_publication    :date
#  preview_description :text
#  description         :text
#  news_image          :string
#  show                :boolean          default(TRUE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class News < ApplicationRecord
  extend FriendlyId
  include Unisender

  self.table_name = 'news'

  after_create :delayed_send_message

  validates :name, :date_publication, :description, :news_image, presence: true
  friendly_id :slug_candidates, use: :slugged

  mount_uploader :news_image, NewsUploader

  def slug_candidates
    [
        :name,
        [
            :name,
            :date_publication
        ]
    ]
  end

  private
  def delayed_send_message
    NewsLetterSenderWorker.perform_async(self.class.name, self.id)
  end

  def self.create_newsletter(id)
    new = News.find(id)
    # слать анонс если дата концерта больше текущей
    if new.date_publication >= Time.now
      ac = ActionController::Base.new
      message_body = ac.render_to_string template: 'news/mail', layout: false, locals: { new: new }
      Unisender.create_email_message new.name, message_body
    end
  end
end
