# == Schema Information
#
# Table name: photo_albums
#
#  id            :integer          not null, primary key
#  name          :string
#  slug          :string
#  description   :text
#  preview_image :string
#  show          :boolean          default(TRUE)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  concert_id    :integer
#
# Indexes
#
#  index_photo_albums_on_concert_id  (concert_id)
#

class PhotoAlbum < ApplicationRecord
  extend FriendlyId

  self.table_name = 'photo_albums'
  validates :name, :preview_image, presence: true

  has_many :photos, dependent: :delete_all
  belongs_to :concert
  accepts_nested_attributes_for :photos, allow_destroy: true

  mount_uploader :preview_image, PhotoAlbumUploader
  friendly_id :name, use: :slugged

end
