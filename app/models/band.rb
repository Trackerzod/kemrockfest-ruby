# == Schema Information
#
# Table name: bands
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  date_based  :date
#  description :text
#  show        :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  band_image  :string
#

class Band < ApplicationRecord
  extend FriendlyId

  self.table_name = 'bands'

  validates :name, presence: true
  friendly_id :name, use: :slugged

  has_many :band_to_band_members, inverse_of: :band, dependent: :destroy
  has_many :band_members, through: :band_to_band_members

  has_many :band_to_genres, inverse_of: :band, dependent: :destroy
  has_many :genres, through: :band_to_genres

  has_many :concert_to_bands, inverse_of: :band, dependent: :destroy
  has_many :concerts, through: :concert_to_bands

  has_many :band_slider_images, dependent: :delete_all
  accepts_nested_attributes_for :band_slider_images, allow_destroy: true

  mount_uploader :band_image, BandUploader
end
