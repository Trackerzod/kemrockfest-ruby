# == Schema Information
#
# Table name: band_slider_images
#
#  id          :integer          not null, primary key
#  image       :string
#  description :text
#  band_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_band_slider_images_on_band_id  (band_id)
#

class BandSliderImage < ApplicationRecord
  validates :image, presence: true

  mount_uploader :image, BandSliderImageUploader
end
