# == Schema Information
#
# Table name: rehearsal_point_slider_images
#
#  id                 :integer          not null, primary key
#  image              :string
#  description        :text
#  rehearsal_point_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_rehearsal_point_slider_images_on_rehearsal_point_id  (rehearsal_point_id)
#

class RehearsalPointSliderImage < ApplicationRecord
  validates :image, presence: true
  mount_uploader :image, RehearsalPointSliderImageUploader
end
