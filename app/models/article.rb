# == Schema Information
#
# Table name: articles
#
#  id                  :integer          not null, primary key
#  name                :string
#  slug                :string
#  date_publication    :date
#  article_image       :string
#  preview_description :text
#  description         :text
#  show                :boolean          default(TRUE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Article < ApplicationRecord
  extend FriendlyId
  include Unisender

  self.table_name = 'articles'

  after_create :delayed_send_message

  validates :name, :date_publication, :article_image, :description, presence: true
  friendly_id :slug_candidates, use: :slugged

  mount_uploader :article_image, ArticleUploader

  def slug_candidates
    [
        :name,
        [
            :name,
            :date_publication
        ]
    ]
  end

  private
  def delayed_send_message
    NewsLetterSenderWorker.perform_async(self.class.name, self.id)
  end

  def self.create_newsletter(id)
    article = Article.find(id)
    # слать анонс если дата концерта больше текущей
    if article.date_publication >= Time.now
      ac = ActionController::Base.new
      message_body = ac.render_to_string template: 'article/mail', layout: false, locals: { article: article }
      Unisender.create_email_message article.name, message_body
    end
  end
end
