# == Schema Information
#
# Table name: band_members
#
#  id           :integer          not null, primary key
#  name         :string
#  music_role   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  member_image :string
#

class BandMember < ApplicationRecord
  extend Enumerize

  self.table_name = 'band_members'

  validates :name, :music_role, presence: true
  enumerize :music_role, in: %w[guitar bass drums keys vocal back_vocal dj]

  has_many :band_to_band_members, inverse_of: :band_member
  has_many :bands, through: :band_to_band_members

  mount_uploader :member_image, BandMemberUploader
end
