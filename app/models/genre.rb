# == Schema Information
#
# Table name: genres
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Genre < ApplicationRecord
  validates :name, presence: true

  self.table_name = 'genres'

  has_many :band_to_genres, inverse_of: :genre
  has_many :bands, through: :band_to_genres
end
