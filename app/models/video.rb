# == Schema Information
#
# Table name: videos
#
#  id               :integer          not null, primary key
#  name             :string
#  slug             :string
#  date_publication :date
#  iframe_link      :text
#  preview_image    :string
#  description      :text
#  show             :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Video < ApplicationRecord
  extend FriendlyId

  validates :name, :date_publication, :preview_image, :iframe_link, presence: true
  friendly_id :name, use: :slugged
  mount_uploader :preview_image, VideoUploader
end
