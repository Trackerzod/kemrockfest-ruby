# == Schema Information
#
# Table name: rehearsal_points
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  description :text
#  address     :string
#  contacts    :string
#  price       :string
#  base_image  :string
#  show        :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  latitude    :float
#  longitude   :float
#

class RehearsalPoint < ApplicationRecord
  extend FriendlyId

  self.table_name = 'rehearsal_points'

  validates :name, :base_image, presence: true
  validates :latitude, numericality: true
  friendly_id :slug_candidates, use: :slugged

  has_many :rehearsal_point_slider_images, dependent: :delete_all
  accepts_nested_attributes_for :rehearsal_point_slider_images

  mount_uploader :base_image, RehearsalPointUploader

  def slug_candidates
    [
        :name,
        [
            :name,
            :address
        ]
    ]
  end
end
