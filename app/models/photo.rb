# == Schema Information
#
# Table name: photos
#
#  id             :integer          not null, primary key
#  photo          :string
#  comment        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  photo_album_id :integer
#
# Indexes
#
#  index_photos_on_photo_album_id  (photo_album_id)
#

class Photo < ApplicationRecord

  self.table_name = 'photos'
  validates :photo, presence: true

  belongs_to :photo_album

  mount_uploader :photo, PhotoUploader

end
