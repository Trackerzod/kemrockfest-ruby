# == Schema Information
#
# Table name: band_to_genres
#
#  id         :integer          not null, primary key
#  band_id    :integer
#  genre_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_band_to_genres_on_band_id   (band_id)
#  index_band_to_genres_on_genre_id  (genre_id)
#

class BandToGenre < ApplicationRecord

  self.table_name = 'band_to_genres'

  belongs_to :band, foreign_key: :band_id
  belongs_to :genre, foreign_key: :genre_id
end
