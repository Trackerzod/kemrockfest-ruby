# == Schema Information
#
# Table name: concert_to_bands
#
#  id         :integer          not null, primary key
#  concert_id :integer
#  band_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_concert_to_bands_on_band_id     (band_id)
#  index_concert_to_bands_on_concert_id  (concert_id)
#

class ConcertToBand < ApplicationRecord

  self.table_name = 'concert_to_bands'

  belongs_to :concert
  belongs_to :band
end
