# == Schema Information
#
# Table name: subscribers
#
#  id         :integer          not null, primary key
#  email      :string
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Subscriber < ApplicationRecord
  extend Enumerize
  include Unisender

  self.table_name = 'subscribers'

  validates :email, presence: true
  validates :email, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: [:create, :update] }

  before_create :set_subscribe
  after_commit :subscriber_in_unisender

  enumerize :status, in: %w[subscribe unsubscribe]

  def subscribed?
    self.status == :subscribe
  end

  def set_subscribe
    self.status = :subscribe
  end

  private
  def subscriber_in_unisender
    if self.status == :subscribe
      Unisender.add_contact_to_list self.email
    else
      Unisender.remove_contact_from_list(self.email)
    end
  end
end
