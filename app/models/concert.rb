# == Schema Information
#
# Table name: concerts
#
#  id           :integer          not null, primary key
#  name         :string
#  slug         :string
#  date_start   :datetime
#  place        :string
#  ticket_price :integer
#  poster_image :string
#  description  :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  show         :boolean          default(TRUE)
#

class Concert < ApplicationRecord
  extend FriendlyId
  include Unisender

  after_create :delayed_send_message

  self.table_name = 'concerts'

  validates :name, :date_start, :place, :poster_image, :ticket_price, presence: true
  friendly_id :name, use: :slugged

  has_many :concert_to_bands, inverse_of: :concert, dependent: :destroy
  has_many :bands, through: :concert_to_bands
  has_one :photo_album

  mount_uploader :poster_image, ConcertUploader

  private
  def delayed_send_message
    NewsLetterSenderWorker.perform_async(self.class.name, self.id)
  end

  def self.create_newsletter(id)
    concert = Concert.find(id)
    # слать анонс если дата концерта больше текущей
    if concert.date_start > Time.now
      ac = ActionController::Base.new
      message_body = ac.render_to_string template: 'concert/mail', layout: false, locals: { concert: concert }
      Unisender.create_email_message concert.name, message_body
    end
  end

end
