module BandHelper

  def get_genres(genres)
    band_genres = ""
    genres.each_with_index do |genre, index|
      band_genres += "<a href='/bands?genre=#{genre.name}'>#{genre.name}</a>"
        if genres.count - 1 != index
          band_genres += ", "
        end
    end
    band_genres.html_safe
  end

end
