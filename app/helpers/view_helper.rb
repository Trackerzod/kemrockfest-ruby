module ViewHelper

# делит весь массив на несколько массивов по "count" элементов
  def split_data(data, count)
    splitted_data = []
    element = []
    data.each_with_index do |item, i|
      element << item
      if element.count == count
        splitted_data << element
        element = []
      elsif ((data.count - 1) - i) < count
        next
      end
    end
    splitted_data << element unless element.empty?
    splitted_data
  end
end
