module ApplicationHelper

  def get_article_type(object)
    case object.class.name
      when 'Article'
        return 'Статьи'
      when 'News'
        return 'Новости'
      when 'Video'
        return 'Видео'
      else
        return 'Статьи'
    end
  end

end
