source 'https://rubygems.org'
ruby '2.5.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use postgres as the database for Active Record
gem 'pg'

# Use Puma as the app server
gem 'puma', '~> 3.0'
gem 'delayed_job_active_record'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# gem 'bootstrap-sass'
gem 'enumerize'
gem 'friendly_id'
gem 'slim-rails'
gem 'honeypot-captcha'
gem 'i18n'
gem 'i18n_generators'
gem 'nokogiri'
gem 'rails_admin'
gem 'rails_admin-i18n'
gem 'russian'
gem 'sidekiq'
gem 'devise'
gem 'simple_form'
gem 'annotate'
gem 'ckeditor'
gem 'mini_magick'
gem 'faker'
gem 'kaminari'
gem 'listen'
gem 'paperclip'
gem 'webpacker', '~> 4.x'

# gem 'sprockets-rails', :require => 'sprockets/railtie'
gem 'carrierwave-imageoptimizer'
gem 'image_optim_rails'
gem 'image_optim_pack'

# Use Capistrano for deployment
group :deployment do
  gem 'capistrano', '~> 3.3.5'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq'
  gem 'capistrano-rsync'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]